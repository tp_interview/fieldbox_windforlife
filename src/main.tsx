import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import './styles/mainStyle.css';
import DarkModeProvider from './context/DarkModeContext';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <BrowserRouter>
            <DarkModeProvider>
                <App />
            </DarkModeProvider>
        </BrowserRouter>
    </React.StrictMode>
);
