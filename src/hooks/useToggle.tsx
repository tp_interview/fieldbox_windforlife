import React, { useState } from 'react';

function useToggle(defaultValue?: boolean) {
    const [value, setValue] = useState(defaultValue);

    function toogleValue(value: boolean) {
        setValue((currentValue) => (typeof value === 'boolean' ? value : !currentValue));
    }

    return [value, toogleValue];
}

export default useToggle;
