import React, { createContext, useState, ReactNode } from 'react';

export type DarkModeContextType = {
    darkMode: boolean;
    toggleDarkMode: () => void;
};

export const DarkModeContext = createContext<DarkModeContextType | null>(null);

function DarkModeProvider({ children }: { children: ReactNode }) {
    const [darkMode, setDarkMode] = useState<boolean>(false);

    function toggleDarkMode(): void {
        setDarkMode(!darkMode);
    }

    return (
        <DarkModeContext.Provider value={{ darkMode, toggleDarkMode }}>
            {children}
        </DarkModeContext.Provider>
    );
}

export default DarkModeProvider;
// export { DarkModeContext, DarkModeProvider };
