import React from 'react';
import './../styles/module.map.css';
import { Location, Marker as MarkerType } from './../assets/types/main';
import { GoogleMap, useLoadScript, MarkerF } from '@react-google-maps/api';
import { useNavigate } from 'react-router-dom';
import anenometerIcon from './../assets/img/anemometer.png';

const defaultCenter: Location = { long: 139.70888, lat: 35.70082 }; // shinjuku

type MapCustomProps = {
    markers?: MarkerType[];
    center?: Location;
    zoom: number;
};

const MapCustom = ({ markers, zoom = 10, center = defaultCenter }: MapCustomProps) => {
    const navigate = useNavigate();
    const { isLoaded } = useLoadScript({
        googleMapsApiKey: import.meta.env.VITE_REACT_GOOGLE_MAPS_API_KEY,
    });

    if (!isLoaded)
        return (
            <section>
                <h2>LOADING...</h2>
            </section>
        );

    return (
        <GoogleMap
            zoom={zoom}
            center={{ lat: center.lat, lng: center.long }}
            mapContainerClassName="map-container"
        >
            {markers &&
                markers.map((marker) => (
                    <MarkerF
                        onClick={() => navigate(`/anenometers/${marker.id}`)}
                        key={marker.id}
                        position={{ lat: marker.loc.lat, lng: marker.loc.long }}
                        icon={{
                            url: anenometerIcon,
                            scaledSize: new google.maps.Size(50, 50),
                            labelOrigin: new google.maps.Point(20, -20),
                        }}
                        label={{
                            text: marker.name,
                            fontFamily: 'Arial',
                            color: '#e84393',
                            fontWeight: 'bold',
                        }}
                    />
                ))}
        </GoogleMap>
    );
};

export default MapCustom;
