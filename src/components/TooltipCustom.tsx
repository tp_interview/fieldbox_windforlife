import React from 'react';
import NavigationIcon from '@mui/icons-material/Navigation';
import { DataTooltipAnenometer } from '../assets/types/main';

type TooltipCustomProps = {
    data: DataTooltipAnenometer;
    label: string;
};

const TooltipCustom = ({ data, label }: TooltipCustomProps) => {
    return (
        <div className="custom-tooltip">
            <h4>{label}</h4>
            <div className="picture">
                <NavigationIcon
                    fontSize="large"
                    style={{
                        padding: '10px',
                        color: '#74b9ff',
                        transform: `rotate(${data.payload.dir}deg) scale(2)`,
                    }}
                />
            </div>

            <div>
                <ul>
                    <li>
                        <label>Force:</label>
                        <span className="relief">{data.value} kn</span>
                    </li>
                    <li>
                        <label>Wind direction:</label>
                        <span className="relief">{data.payload.dir} °</span>
                    </li>
                </ul>
            </div>
            {/* <pre>{JSON.stringify(payload, null, 4)}</pre> */}
        </div>
    );
};

export default TooltipCustom;
