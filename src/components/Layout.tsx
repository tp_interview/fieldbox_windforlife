import React, { ReactNode } from 'react';
import Header from './Header';
import packageJson from './../../package.json';

type LayoutProps = {
    children: ReactNode;
};

const Layout = ({ children }: LayoutProps) => {
    return (
        <div>
            <Header />
            <main>{children}</main>
            <footer>Fieldbox - WindForLife v{packageJson.version} by FCH @ 2023</footer>
        </div>
    );
};

export default Layout;
