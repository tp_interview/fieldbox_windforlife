import React from 'react';
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
    Label,
} from 'recharts';

import { DataElt, Serie } from './../assets/types/main';
import './../styles/module.chart.css';
import TooltipCustom from './TooltipCustom';

type ChartCustomProps = {
    data: DataElt[] | undefined;
    series: Serie[];
};

const ChartCustom = ({ data, series }: ChartCustomProps) => {
    const CustomTooltip = ({ active, payload, label }: any) => {
        if (active && payload && payload.length) {
            const data = payload[0];

            return <TooltipCustom label={label} data={data} />;
        }

        return null;
    };

    return (
        <ResponsiveContainer height="100%" width="100%">
            <LineChart
                data={data}
                margin={{
                    top: 0,
                    right: 0,
                    left: 0,
                    bottom: 0,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name"></XAxis>
                <YAxis>
                    <Label
                        style={{
                            textAnchor: 'middle',
                            fontSize: '100%',
                            fill: '#0984e3',
                            fontStyle: 'italic',
                        }}
                        angle={270}
                        value={'Force (knots)'}
                    />
                </YAxis>
                {/* <Tooltip /> */}
                <Tooltip content={<CustomTooltip />} />
                <Legend />
                {series.map((serie) => (
                    <Line
                        key={serie.field}
                        type="monotone"
                        dataKey={serie.field}
                        stroke={serie.color}
                        activeDot={{ r: 8 }}
                    />
                ))}
            </LineChart>
        </ResponsiveContainer>
    );
};

export default ChartCustom;
