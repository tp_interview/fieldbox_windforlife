import React, { useContext } from 'react';
import './../styles/module.header.css';
import { Link } from 'react-router-dom';
import NightlightIcon from '@mui/icons-material/Nightlight';
import Brightness5OutlinedIcon from '@mui/icons-material/Brightness5Outlined';
import { DarkModeContext, DarkModeContextType } from '../context/DarkModeContext';
import { IconButton } from '@mui/material';

const Header = () => {
    const { darkMode, toggleDarkMode } = useContext(DarkModeContext) as DarkModeContextType;

    return (
        <header>
            <nav>
                <Link to="/">
                    <span className="logo-title">Wind For Life</span>
                </Link>
            </nav>
            <IconButton onClick={toggleDarkMode}>
                {darkMode ? (
                    <Brightness5OutlinedIcon style={{ color: '#FFF' }} />
                ) : (
                    <NightlightIcon />
                )}
            </IconButton>
        </header>
    );
};

export default Header;
