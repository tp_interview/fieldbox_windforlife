import axios from 'axios';
import React, { useEffect, useState } from 'react';
import MapCustom from '../components/MapCustom';
import { Marker, Location } from './../assets/types/main';

// FUNCTION:
// - The home page shows an interactive map with all anemometers.
// - Clicking on an anemometer opens a detailed view

const Home = () => {
    const [markers, setMarkers] = useState<Marker[] | undefined>();
    const [mapCenter, setMapCenter] = useState<Location | undefined>();

    // Load anenometers
    async function loadAnenometers() {
        const res = await axios.get(`${import.meta.env.VITE_REACT_URI_API}anenometersList`);
        if (res.status === 200) {
            const data = await res.data;
            setMarkers(data);

            // Set map center : 1st location in array
            if (data.length > 0) {
                const firstLocation: Marker = data[0];
                setMapCenter({ lat: firstLocation.loc.lat, long: firstLocation.loc.long });
            }
        }
    }

    useEffect(() => {
        // Avoid to have twice call
        return () => {
            loadAnenometers();
        };
    }, []);

    if (!mapCenter) return <></>;

    return <MapCustom zoom={9} markers={markers} center={mapCenter} />;
};

export default Home;
