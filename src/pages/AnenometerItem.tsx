import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import { AnenometerData } from '../assets/types/main';
import { WindData } from '../assets/types/main';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import TodayIcon from '@mui/icons-material/Today';
import DateRangeIcon from '@mui/icons-material/DateRange';
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import ChartCustom from '../components/ChartCustom';
import { IconButton, Tooltip, Grid } from '@mui/material';

const AnenometerItem = () => {
    const params = useParams();
    const [idAnenometer] = useState<number | undefined>(params.id ? +params.id : undefined);
    const [anenometer, setAnenometer] = useState<AnenometerData | undefined>();
    const [dataChart, setDataChart] = useState();

    // Load anenometers Detail information
    async function loadAnenometerDetail() {
        const res = await axios.get(`${import.meta.env.VITE_REACT_URI_API}anenometersDetail`);
        if (res.status === 200) {
            const data = await res.data;
            const anenometer = data.find((elt: AnenometerData) => elt.id === idAnenometer);
            setAnenometer(anenometer);

            const temp = anenometer.readings;

            // Sort dates ascending
            temp.sort((a: WindData, b: WindData) => {
                const d1 = new Date(a.timestamp);
                const d2 = new Date(b.timestamp);
                return d1.getTime() - d2.getTime();
            });
            new Intl.DateTimeFormat('en-US');

            setDataChart(
                temp.map((elt: WindData) => ({
                    name: new Intl.DateTimeFormat('fr-FR').format(new Date(elt.timestamp)),
                    ...elt,
                }))
            );
        }
    }

    useEffect(() => {
        // Avoid to have twice call
        return () => {
            loadAnenometerDetail();
        };
    }, []);

    return (
        <section>
            <Grid container spacing={2}>
                {/* INFORMATION */}
                <Grid item xs={12} md={4}>
                    <h1 className="title-icon">
                        <label>{anenometer?.name} </label>
                        <Link to="/">
                            <Tooltip title="Retour">
                                <IconButton size="large">
                                    <ArrowBackIosNewIcon />
                                </IconButton>
                            </Tooltip>
                        </Link>
                    </h1>

                    <div className="container-info">
                        <h3>
                            <LocationOnIcon /> Coordinates
                        </h3>
                        <ul className="data">
                            <li>
                                <label>Latitude :</label>{' '}
                                <span className="relief">{anenometer?.loc.lat}</span>
                            </li>
                            <li>
                                <label>Longitude :</label>
                                <span className="relief">{anenometer?.loc.long}</span>
                            </li>
                        </ul>
                    </div>

                    <div className="container-info">
                        <h3>
                            <QueryStatsIcon /> Statistics
                        </h3>

                        <div className="container-data">
                            <div className="data">
                                <TodayIcon fontSize="large" />
                                <h4>Today average wind force</h4>
                                <div className="value">
                                    {anenometer?.statistics.average.daily.force} kn
                                </div>
                            </div>
                            <div className="data">
                                <DateRangeIcon fontSize="large" />
                                <h4>Week average wind force </h4>
                                <div className="value">
                                    {anenometer?.statistics.average.weekly.force} kn
                                </div>
                            </div>
                        </div>
                    </div>
                </Grid>

                {/* CHART */}
                <Grid item xs={12} md={8} style={{ minHeight: '500px' }}>
                    <ChartCustom data={dataChart} series={[{ field: 'force', color: '#e84393' }]} />
                </Grid>
            </Grid>

            {/* <h3>DATA CHART</h3>
            <pre>{JSON.stringify(dataChart, null, 4)}</pre>

            <h3>DATA FULL</h3>
            <pre>{JSON.stringify(anenometer, null, 4)}</pre> */}
        </section>
    );
};

export default AnenometerItem;
