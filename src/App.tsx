import React, { useContext } from 'react';
import { Route, Routes } from 'react-router-dom';
import AnenometerItem from './pages/AnenometerItem';
import Home from './pages/Home';
import Layout from './components/Layout';
import { DarkModeContext, DarkModeContextType } from './context/DarkModeContext';

function App() {
    const { darkMode } = useContext(DarkModeContext) as DarkModeContextType;

    return (
        <div className={darkMode ? 'dark-mode' : ''}>
            <Routes>
                <Route
                    path="/"
                    element={
                        <Layout>
                            <Home />
                        </Layout>
                    }
                />
                <Route
                    path="/anenometers/:id"
                    element={
                        <Layout>
                            <AnenometerItem />
                        </Layout>
                    }
                />
            </Routes>
        </div>
    );
}

export default App;
