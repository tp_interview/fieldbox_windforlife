export type Location = {
    long: number;
    lat: number;
};

export type Marker = {
    id: number;
    loc: Location;
    name?: string;
};

export type AnenometerData = {
    id: number;
    loc: Location;
    name: string;
    readings: WindData[];
    statistics: Statistics;
};

type Statistics = {
    average: {
        daily: {
            force: number;
        };
        weekly: {
            force: number;
        };
    };
};

export type WindData = {
    dir: number;
    force: number;
    timestamp: Date;
};

export type DataElt = {
    name: string;
    [key: string]: string | number;
};

export type Serie = {
    field: string;
    color: string;
};

export type DataTooltipAnenometer = {
    payload: WindData;
    value: number | string;
};
