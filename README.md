# <h1 style="color:#0984e3;font-weight:bold">Project : WIND FOR LIFE</style>

## Project description:
> Build a react web application composed by 2 pages:
- Home page : interactive map (google map) that lists all anenometers
- Anenometer detailed page

## Get started
1. First launch : npm install or yarn install
2. Launch MOCK API by executing the bellow command on Terminal : 
```
json-server --watch src/api/db.json
```
3. Launch web application
```
npm dev
```
<div style="background-color: #ffeaa7;padding:10px;color:#000;border-radius:4px">
<div><u>NB:</u></div>
<p>This project is in development. So in order to use the google map API KEY, you have to use the url <span style="color:#e84393;font-weight:600">http://localhost:5173/</span> on local while running WIND FOR LIFE app</p>
</div>

## Mock API
> **npm package : json-server**

Global installation of json-server package for emulating a local REST API
1. npm install -g json-server
2. json-server --watch src/api/db.json

## Project architecture:
![WIND FOR LIFE](/input/WIND_FOR_LIFE_DIAGRAM.jpg "San Juan Mountains")
